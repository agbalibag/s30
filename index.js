/*
1. Create a folder called s30
2. Make a file called index.js
3. Open your terminal to s30
4. Login to MongoDB Atlas
5. inside s30 npm init
6. npm install express
*/

//7. import express
const express = require('express')
	//import mongoose(step 13)
const mongoose = require('mongoose');
//8. create express app
const app = express();

//9. create a const called PORT
const port = 3001;

//10. setup server to handle data from request
	//allows our app to read JSON data
app.use(express.json())

	//allows our app to read data from forms
app.use(express.urlencoded({extended:true}));

//11. add listen method for requests
// app.listen(port, ()=> console.log(`Server running at port ${port}`));

//12. install mongoose : npm install mongoose
//Mongoose is a package that allows creation of Schemas to model our data structures

//13. import mongoose (nasa taas)
//const mongoose = require('mongoose');

//14. go to MongoDB ATlas and change the network access to anywhere(0.0.0.0)

//15. get connection string and change the password to your password(admin1234)
//mongodb+srv://admin:admin1234@zuitt-bootcamp.upmgf.mongodb.net/myFirstDatabase?retryWrites=true&w=majority

//16. change myFirstDatabase to s30, MongoDB will automatically create database for us
//mongodb+srv://admin:admin1234@zuitt-bootcamp.upmgf.mongodb.net/s30?retryWrites=true&w=majority

//17. Connecting to MongoDB ATlas - add .connect() method
//mongoose.connect();

//18a add the conection string as the 1st argument
//18b add this object to allow connection (add comma after 'majority')
/*
{
	userNewUrlParser : true
	useUnifiedTopology : true
}
*/
mongoose.connect('mongodb+srv://admin:admin1234@zuitt-bootcamp.upmgf.mongodb.net/s30?retryWrites=true&w=majority',
	{
	useNewUrlParser : true,
	useUnifiedTopology : true
	}
);

//19. Set notification for connection success or failure by using conection property of mongoose
//mongoose.connection;

//20. store it in a variable called db
let db = mongoose.connection;

//21. console.error.bind(console) allows us to print errors
db.on("error", console.error.bind(console, "connection error"));

//22. if the connection is successful, output this console
db.once("open", ()=> console.log("We're connected to the cloud database"))

//23. Schemas determine  the structure of the documents to be written in the database
//Schemas act as blueprints to our data
//Use the Schema() constructor of the Mongoose module to create a new Schema object
const taskSchema = new mongoose.Schema({
	//Define the fields with the corresponding data type
	//For a task, it needs a "task name" and "task status"
	//There is a field called "name" and its data type "String"
	name: String,
	//There is a field called "status" that is a "String" and the default value is "pending"
	status: {
		type: String,
		default: "pending"
	}
});

//Models use Schemas and they act as the middleman from the server(JS Code) to our database
//24. Create a model
//Models must be in singular form and capitalized
//The first parameter of the Mongoose model method indicates the collection in where to store the data
//name of the collection: tasks (plural form of the model)


//The second parameter is used to specify the Schema/blueprint
//of the documents that will be stored in the MongoDB collection

//Using Mongoose, the package was programmed well enough that it
//automatically converts the singular form of the model name
//into a plural form when creating a collection in postman
const Task = mongoose.model("Task", taskSchema)

/*
Business Logic
1. add a functionality to check if there are duplicate tasks
	-if the task already exists in the database, wwwe return an error
	-if the task doesn't exist in the database, we add it in the database
*/

//25. Create route to add task
// app.post('/tasks', (req, res)=>{

// })

//26. Check if the task already exists
//Use the Task model to interact with the tasks collection
app.post('/tasks', (req, res)=>{
	Task.findOne({name: req.body.name}, (err, result)=>{
		if(result != null && result.name == req.body.name){
			//Return a message to the client/Postman
			return res.send('Duplicate task found')
		}else{
			let newTask = new Task({
				name: req.body.name
			})

			newTask.save((saveErr, savedTask)=>{
				if(saveErr){
					return console.error(saveErr)
				}else{
					return res.status(201).send('New task created')
				}

			})
		}
	})
})

/*example(explanation for line 120-122):
tasks collection:
{
	"name": "eat",
	"name": "sleep"
	"name":"code"
}
postman:
"name":"code"

**the result of findOne is "name":"code"
and if true, will return a message to the client/Postman:
			return res.send('Duplicate task found')
*/

//27. Get all  tasks: 
app.get('/tasks', (req, res)=>{
	//Get all the contents of the task collection via the Task model
	//"find" is a Mongoose method that is similar to Mongodb "find", and an empty "{}" means it returns all the documents and stores them in the "result" parameter of the callback function
	Task.find({}, (err, result)=>{
		if(err){
			return console.log(err)
		}else{
			return res.status(200).json({data:result})
		}
	})
})

//Activity
//1. Create a User schema 
const userSchema = new mongoose.Schema({
	username: String,
	password: String,
});

//2. Make a model
const User = mongoose.model("User", userSchema)

//3.

//3a
app.post('/signup', (req, res)=>{
	User.findOne({username: req.body.username}, (err, result)=>{
		if(result != null && result.username == req.body.username){
			return res.send('Duplicate username found')
		}else{
		
			let newUser = new User({
				username: req.body.username
			})

			newUser.save((saveErr, savedUser)=>{
				if(saveErr){
					return console.error(saveErr)
				}else{
					return res.status(201).send('New user created')
				}

			})
		}
	})
})



//Step 11:
app.listen(port, ()=> console.log(`Server running at port ${port}`));